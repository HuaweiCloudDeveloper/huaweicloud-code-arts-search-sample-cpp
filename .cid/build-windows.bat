@echo off

if exist .\build (rd /S /Q .\build)
mkdir build
pushd .\build
cmake --no-warn-unused-cli -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_BUILD_TYPE:STRING=Debug -GNinja -S .. -B .
ninja
popd

del /F /Q *.tar.gz

SET datetime=%DATE:~0,4%%DATE:~5,2%%DATE:~8,2%%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%
SET datetime=%datetime: =0%
SET buildVersion=%datetime%-Windows
tar -czf demo%buildVersion%.tar.gz build