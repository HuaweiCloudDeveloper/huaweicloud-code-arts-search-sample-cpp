#ifndef CPP_MYWIDGET_H
#define CPP_MYWIDGET_H

#include <vector>
#include "QtWidgets/QWidget"
#include "QtWidgets/QApplication"
#include "QtWidgets/QLabel"
#include "QtWidgets/QPushButton"
#include "QtWidgets/QVBoxLayout"
#include "QtWidgets/QLineEdit"
#include "QtWidgets/QStackedWidget"
#include "QtWebKitWidgets/QWebView"
#include "QtCore/QDebug"


class MyWidget : public QWidget
{
    Q_OBJECT
public:
    MyWidget(QWidget *parent = nullptr);

private slots:

    void ShowUrlLineEdit();

    void ShowUrlPage();

    void OpenLink(const QUrl& url);

private:
    QPushButton *m_searchButton = nullptr;
    QWidget *m_menuWidget = nullptr;
    QStackedWidget *m_stackWidget = nullptr;
    QLineEdit *m_urlLineEdit = nullptr;
    QLabel *m_menuTitle = nullptr;
    QVBoxLayout *m_menuButtonLayout = nullptr;
};

#endif //CPP_MYWIDGET_H