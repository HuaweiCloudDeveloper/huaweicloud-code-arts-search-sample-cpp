
## 1、介绍 

CodeArts IDE面向华为云开发者提供的智能化可扩展桌面集成开发环境（IDE），结合华为云行业和产业开发套件，实现极致的一站式用云和开发体验。
- **连接华为云，用云更畅快**：内置华为云帐号支持和华为云API开发套件，支持一键登录后快速用云，浏览、查找、引用和调试云服务API，并有样例代码支持

- **编码新体验，开发更高效**：内置华为自研C/C++语言开发支持，提供全新的工程加载、语法着色、符号解析、编码重构和运行调试等开发体验，提升开发效率

- **能力可扩展，生态更开放**：支持基于插件的能力扩展，开放的插件标准，开源的插件框架，开放的插件市场，形成更加开放的生态系统
  
- **界面可裁剪，体验更优质**：支持基于组建的界面剪裁，在精简模式下形成专用工具的优质体验，又可以在需要时升级为全模式的全量IDE工具


**CodeArts IDE for C/C++**：基于C/C++语言开发CMake工程，并通过CodeArts IDE完成从工程创建、代码编写、运行调试到发布测试的全过程。基于插件扩展可以将个人开发者作业流集成其中，实现从需求到提交的全部过程，更可在业务中集成华为云所提供的的诸多能力，提升应用交付效率。

### 您将学到什么？

本实验将指导开发者通过CodeArts IDE for C/C++平台，在本地桌面快速开发一个基于Qt实现的简单项目。通过本实验您将体验到：

- 1.在CodeArts IDE for C/C++上进行基于CMake项目的本地编译构建
- 2.在CodeArts IDE上调试和运行
- 3.实现一个基于Qt的网页搜索框功能，浏览网页


## 2、前置条件

- 注册并登陆华为云账号
- 安装[CodeArts IDE for C/C++](https://devcloud.cn-north-4.huaweicloud.com/codeartside/home)


## 3、编译构建与运行调试
通过CodeArts IDE以文件夹的形式打开代码包

在IDE页面的右上角，点击构建项目按钮，或在页面上方的构建->构建项目，CMake构建类型选择Debug。
![img.png](assets/../asserts/1.png)
然后在弹窗中选择all META，当编译构建成功后，日志打印finished，并显示成功的图示。
![img.png](assets/../asserts/2.png)

构建成功后，在页面的右上角选择调试的目标，然后进行调试和运行。
![img.png](assets/../asserts/3.png)

更多信息请参考CodeArts IDE [用户指南](https://support.huaweicloud.com/usermanual-codeartside/codeartside_01_0048.html)

## 4、关键代码片段
```cpp

int main(int argc, char *argv[])
{   
    QApplication app(argc, argv);
    MyWidget widget;
    widget.show();
    return app.exec(); 
}

MyWidget::MyWidget(QWidget *parent) : QWidget(parent)
{
    resize(800, 600);
    m_menuButtonLayout = new QVBoxLayout;
    m_menuButtonLayout->setAlignment(Qt::AlignTop);
    m_menuTitle = new QLabel(this);
    m_menuTitle->setText("Menu");
    m_menuTitle->setAlignment(Qt::AlignCenter);

    m_searchButton = new QPushButton("Search", this);
    m_searchButton->setFixedSize(140, 30);
    m_menuButtonLayout->addWidget(m_menuTitle);
    m_menuButtonLayout->addWidget(m_searchButton);
    m_menuWidget = new QWidget(this);
    m_menuWidget->setLayout(m_menuButtonLayout);

    QFrame* line = new QFrame(this);
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);

    m_stackWidget = new QStackedWidget(this);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(m_menuWidget, 1);
    mainLayout->addWidget(line);
    mainLayout->addWidget(m_stackWidget, 4);

    setLayout(mainLayout);
    connect(m_searchButton, &QPushButton::clicked, this, &MyWidget::ShowUrlLineEdit);
}

void MyWidget::ShowUrlLineEdit()
{
    if (!m_urlLineEdit) {
        m_urlLineEdit = new QLineEdit(this);
        m_urlLineEdit->setPlaceholderText("Enter URL");
        m_urlLineEdit->setFixedHeight(40);
        m_urlLineEdit->setAlignment(Qt::AlignHCenter);
        connect(m_urlLineEdit, &QLineEdit::returnPressed, this, &MyWidget::ShowUrlPage);
    }
    m_stackWidget->addWidget(m_urlLineEdit);
    m_stackWidget->setCurrentWidget(m_urlLineEdit);
    m_urlLineEdit->show();
    m_urlLineEdit->setFocus();
    return;
}

void MyWidget::ShowUrlPage()
{
    QString url = m_urlLineEdit->text();
    if (!url.isEmpty()) {
        QWebView *webview = new QWebView(this);
        webview->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
        connect(webview, &QWebView::linkClicked, this, &MyWidget::OpenLink);
        webview->load(QUrl(url));
        m_stackWidget->addWidget(webview);
        m_stackWidget->setCurrentWidget(webview);
    }
    return;
}

void MyWidget::OpenLink(const QUrl& url)
{
    QWebView *webview = qobject_cast<QWebView*>(sender());
    if (webview) {
        webview->load(url);
    }
    return;
}

```
## 5、运行结果

运行结果如下，输入网址

![img.png](assets/../asserts/4.png)

浏览网页内容

![img.png](assets/../asserts/5.png)


#### 注意: 如果无法正常编译，请检查3rdpart/qt/bin/目录下的Qt5WebKit.dll是否被解压

## 6、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/usermanual-codeartside/codeartside_01_0042.html)

## 7、修订记录
   |  发布日期  |  文档版本 |  修订说明 |
   | ------------ | ------------ | ------------ |
   |  2023-08-14 |  1.0 |  文档首次发布 |